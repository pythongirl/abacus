import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"
import "CoreLibs/frameTimer"

local gfx <const> = playdate.graphics

playdate.display.setRefreshRate(50)

-- Assets
local frametimeFont = gfx.font.new("fonts/Frametime")
assert(frametimeFont, "failed to load frametimeFont")

local beadTexture = gfx.image.new("images/Bead")
assert(beadTexture, "failed to load beadTexture")
local backgroundTexture = gfx.image.new("images/Background")
assert(backgroundTexture, "Failed to load backgroundTexture")
local cursorTexture = gfx.image.new("images/Cursor")
assert(cursorTexture, "failed to load cursorTexture")

-- Sprites
gfx.sprite.setBackgroundDrawingCallback(function()
    backgroundTexture:draw(0, 0)
end)

-- Bead Lines
BeadLine = {}
class("BeadLine").extends()
function BeadLine:init(line, numBeads)
    self.value = 0
    self.numBeads = numBeads or 9
    self.beads = {}
    for i = 0, 8 do
        local beadSprite = gfx.sprite.new(beadTexture)
        assert(beadSprite, "failed to create beadSprite")
        beadSprite:setCenter(0, 0)
        beadSprite:moveTo(self:beadRight(i + 1), 240 - 32 - 32 * line)
        beadSprite:add()

        table.insert(self.beads, beadSprite)
    end
end

function BeadLine:beadLeft(i)
    return 8 + 16 * (i - 1)
end

function BeadLine:beadRight(i)
    return 400 - 9 - 16 * (self.numBeads - i + 1)
end

function BeadLine:moveBeadsRight(i, j)
    j = j or i
    local n = j - i + 1

    local t = playdate.frameTimer.new(
        12,
        self:beadLeft(i),
        self:beadRight(i),
        playdate.easingFunctions.inQuad
    )
    t.updateCallback = function(timer)
        for b = 0, n - 1 do
            local currentBead = self.beads[i + b]
            currentBead:moveTo(timer.value + 16 * b, currentBead.y)
        end
    end
end

function BeadLine:moveBeadsLeft(i, j)
    j = j or i
    local n = j - i + 1

    local t = playdate.frameTimer.new(
        12,
        self:beadRight(i),
        self:beadLeft(i),
        playdate.easingFunctions.inQuad
    )
    t.updateCallback = function(timer)
        for b = 0, n - 1 do
            local currentBead = self.beads[i + b]
            currentBead:moveTo(timer.value + 16 * b, currentBead.y)
        end
    end
end

function BeadLine:incrementValue()

end

function BeadLine:decrementValue()
end

function BeadLine:reset()
    self:moveBeadsRight(1, self.value)
    self.value = 0
end

-- Main state class
Abacus = {}
class("Abacus").extends()

function Abacus:init()
    self.currentLine = 0

    self.beadLines = {}
    for i = 0, 6 do
        table.insert(self.beadLines, BeadLine(i))
    end

    -- Cursor
    self.cursor = gfx.sprite.new(cursorTexture)
    assert(self.cursor, "failed to create cursorSprite")
    self.cursor:add()

    self.flippedCursor = gfx.sprite.new(cursorTexture)
    assert(self.flippedCursor, "failed to create cursorSprite")
    self.flippedCursor:setImageFlip(gfx.kImageFlippedX)
    self.flippedCursor:add()

    self:setLine(0)
end

function Abacus:reset()
    for i = 1, 7 do
        self.beadLines[i]:reset()
    end
end

function Abacus:setLine(i)
    self.currentLine = i
    self.cursor:moveTo(5, 240 - 16 - 32 * i)
    self.flippedCursor:moveTo(400 - 5, 240 - 16 - 32 * i)
end

function Abacus:value()
    local value = 0
    for i = 1, 7 do
        value = value + self.beadLines[i].value * 10 ^ (i - 1)
    end
    return value
end

function Abacus:incrementLine(i)
    i = i or self.currentLine
    if i > 6 then return end

    local line = self.beadLines[i + 1]

    if line.value == 9 then
        line.value = 0
        line:moveBeadsRight(1, 9)
        self:incrementLine(i + 1)
    else
        line.value = line.value + 1
        line:moveBeadsLeft(line.value)
    end
end

function Abacus:decrementLine(i)
    i = i or self.currentLine
    if i > 6 then return end
    local line = self.beadLines[i + 1]

    if line.value == 0 then
        line.value = 9
        line:moveBeadsLeft(1, 9)
        self:decrementLine(i + 1)
    else
        line:moveBeadsRight(line.value)
        line.value = line.value - 1
    end
end

local abacus = Abacus()

local accumulated_angle = 0


local function drawFrameTime(x, y)
    local frame_time = playdate.getElapsedTime()

    gfx.setImageDrawMode(playdate.graphics.kDrawModeInverted)

    gfx.setColor(gfx.kColorBlack)
    gfx.fillRect(x, y, 32, 12)

    frametimeFont:drawText(string.format("%.4g", frame_time * 1000), x + 1, y + 1);
    gfx.setImageDrawMode(playdate.graphics.kDrawModeCopy)
end

function playdate.update()
    playdate.resetElapsedTime()


    if playdate.buttonJustPressed(playdate.kButtonUp) and abacus.currentLine < 6 then
        abacus:setLine(abacus.currentLine + 1)
    end

    if playdate.buttonJustPressed(playdate.kButtonDown) and abacus.currentLine > 0 then
        abacus:setLine(abacus.currentLine - 1)
    end

    if playdate.buttonJustPressed(playdate.kButtonLeft) then
        abacus:incrementLine()
    end

    if playdate.buttonJustPressed(playdate.kButtonRight) then
        abacus:decrementLine()
    end

    if playdate.buttonJustPressed(playdate.kButtonB) then
        abacus:reset()
    end

    playdate.frameTimer.updateTimers()
    gfx.sprite.update()

    gfx.setImageDrawMode(playdate.graphics.kDrawModeInverted)
    gfx.drawText(string.format("Value: *%d*", abacus:value()), 64, 0)
    gfx.setImageDrawMode(playdate.graphics.kDrawModeCopy)


    -- Draw fps counter
    -- playdate.drawFPS(0, 0)
    drawFrameTime(0, 0)
end
